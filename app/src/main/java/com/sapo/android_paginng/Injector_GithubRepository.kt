package com.sapo.android_paginng

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object Injector

private fun Injector.getGithubLocalCache(context: Context): GithubLocalCache {
    return GithubLocalCache.getInstance(
        AppDatabase.getInstance(context.applicationContext).repoDao())
}

private fun Injector.getGithubRemoteService() : GithubRemoteServices {
    return GithubRemoteServices(provideApi())
}

private fun Injector.provideApi() : GithubApi = Retrofit.Builder()
    .baseUrl("https://api.github.com/")
    .addConverterFactory(GsonConverterFactory.create())
    .build()
    .create(GithubApi::class.java)


private fun Injector.getGithubRepository(context: Context) : GithubRepository {
    return GithubRepository(getGithubRemoteService(), getGithubLocalCache(context))
}
fun Injector.provideSearchRepositoriesViewModel(
    context: Context
//argus: String
): SearchRepositoriesViewModelFactory {
    val repository = getGithubRepository(context)
    return SearchRepositoriesViewModelFactory(repository)
}