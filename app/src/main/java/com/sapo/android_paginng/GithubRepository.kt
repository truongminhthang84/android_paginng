package com.sapo.android_paginng

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder

class GithubRepository(
    private val service: GithubRemoteServices,
    private val cache: GithubLocalCache
) {
    companion object {
        private const val DATABASE_PAGE_SIZE = 20
    }

    fun getRepos(user: String) = service.getRepos(user)
    fun getReposWithQuery() = service.search()
    fun search(query: String) : RepoSearchResult {
        val dataSourceFactory = cache.reposByName(query)
        val repoBoundaryCallback = RepoBoundaryCallback(query, service, cache)
        val networkErrors = repoBoundaryCallback.networkErrors

        val data = LivePagedListBuilder(dataSourceFactory, DATABASE_PAGE_SIZE)
            .setBoundaryCallback(repoBoundaryCallback)
            .build()

        return RepoSearchResult(data, networkErrors)

    }
}
