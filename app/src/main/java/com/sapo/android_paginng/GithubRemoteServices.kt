package com.sapo.android_paginng

import android.accounts.NetworkErrorException
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface GithubApi {
    @GET("users/{user}/repos")
    suspend fun getRepos(@Path("user") user: String): List<Repo>

    @GET("search/repositories?sort=stars")
    suspend fun searchRepos(
        @Query("q") query: String,
        @Query("page") page: Int,
        @Query("per_page") itemsPerPage: Int
    ): RepoSearchResponse
}

class GithubRemoteServices(private val api: GithubApi) {

    fun getRepos(user: String): LiveData<Result<List<Repo>>> = liveData {
        try {
            val items = api.getRepos(user)
            emit(Result.success(items))
        } catch (e: Exception) {
            emit(Result.failure(NetworkErrorException(e.message)))
        }
    }

    fun search(query: String = "Android", page: Int = 0, itemsPerPage: Int = 50): LiveData<Result<List<Repo>>> = liveData {
            try {
                val response = api.searchRepos(query, page, itemsPerPage)
                Log.i("searchsex", "${response.items}")
                emit(Result.success(response.items))
            } catch (e: Exception) {
                emit(Result.failure(NetworkErrorException(e.message)))
                Log.i("searchsexError", "${e.message}")

            }
        }


}


