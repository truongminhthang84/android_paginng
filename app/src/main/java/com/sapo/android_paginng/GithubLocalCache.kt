package com.sapo.android_paginng

import androidx.lifecycle.LiveData
import androidx.paging.DataSource

class GithubLocalCache (private val repoDao: RepoDao
) {

    val getAll: LiveData<List<Repo>> = repoDao.getAll()

    suspend fun insert(repo: Repo) {
        repoDao.insert(repo)
    }

    suspend fun insert(repos: List<Repo>) {
        repoDao.insertAll(repos)
    }

    fun reposByName(name: String): DataSource.Factory<Int, Repo> {
        // appending '%' so we can allow other characters to be before and after the query string
        val query = "%${name.replace(' ', '%')}%"
        return repoDao.reposByName(query)
    }
    companion object {

        // For Singleton instantiation
        @Volatile private var instance: GithubLocalCache? = null

        fun getInstance(repoDao: RepoDao) =
            instance ?: synchronized(this) {
                instance ?: GithubLocalCache(repoDao).also { instance = it }
            }
    }
}