package com.sapo.android_paginng



import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*

@Dao
interface RepoDao {
    @Query("SELECT * from repo_table ORDER BY id ASC")
    fun getAll() : LiveData<List<Repo>>

    @Query("SELECT * FROM repo_table WHERE (name LIKE :queryString)")
    fun reposByName(queryString: String): DataSource.Factory<Int,Repo>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(repo: Repo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrReplaceIfNeed(repo: Repo)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(repo: List<Repo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllOrReplaceAllIfNeed(repo: List<Repo>)

    @Delete
    suspend fun delete(repo: Repo)

    @Query("DELETE FROM repo_table")
    suspend fun deleteAll()
}




