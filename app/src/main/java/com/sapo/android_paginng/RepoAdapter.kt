package com.sapo.android_paginng


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sapo.android_paginng.databinding.RepoItemBinding

/**
 * Adapter for the [RecyclerView] in [SearchRepositoriesFragment].
 */
class RepoAdapter : ListAdapter<Repo, RecyclerView.ViewHolder>(RepoDiffCallback()) {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val repo = getItem(position)
        (holder as RepoViewHolder).bind(repo)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return RepoViewHolder(RepoItemBinding.inflate(
            LayoutInflater.from(parent.context), parent, false))
    }

    class RepoViewHolder(
        private val binding: RepoItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setOnClickListener {
                binding.repo?.let { repo ->
                    navigateToPlant(repo, it)
                }
            }
        }

        private fun navigateToPlant(
                repo: Repo,
            it: View
        ) {

        }

        fun bind(item: Repo) {
            binding.apply {
                repo = item
                executePendingBindings()
            }
        }
    }
}

private class RepoDiffCallback : DiffUtil.ItemCallback<Repo>() {

    override fun areItemsTheSame(oldItem: Repo, newItem: Repo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Repo, newItem: Repo): Boolean {
        return oldItem == newItem
    }
}