package com.sapo.android_paginng

import android.content.Context
import androidx.lifecycle.*
import androidx.paging.PagedList


class SearchRepositoriesViewModel(private val repository: GithubRepository) : ViewModel() {
    // TODO: Implement the ViewModel

    val queryLiveData = MutableLiveData("android")

    val lastQueryValue: String?
        get() = queryLiveData.value

    private val repoResult: LiveData<RepoSearchResult> = queryLiveData.map {
        repository.search(it)
    }

//    val repos: LiveData<PagedList<Repo>> = Transformations.switchMap(repoResult) {
//        it.data
//    }
//
//    val networkErrors: LiveData<String> = Transformations.switchMap(repoResult) {
//        it.networkErrors
//    }
    val repos: LiveData<Result<List<Repo>>> = repository.getReposWithQuery()


}

class SearchRepositoriesViewModelFactory internal constructor(
    private val repository: GithubRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SearchRepositoriesViewModel(repository) as T
    }
}

