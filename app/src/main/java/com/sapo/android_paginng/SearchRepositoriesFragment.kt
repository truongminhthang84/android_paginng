package com.sapo.android_paginng


import android.accounts.NetworkErrorException
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.work.ListenableWorker
import com.sapo.android_paginng.databinding.FragmentSearchRepositoriesBinding

/**
 * A simple [Fragment] subclass.
 */

open class BaseFragment : Fragment()

class SearchRepositoriesFragment : BaseFragment() {

    private lateinit var binding: FragmentSearchRepositoriesBinding

    private val viewModel: SearchRepositoriesViewModel by viewModels {
        Injector.provideSearchRepositoriesViewModel(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val adapter = RepoAdapter()
        binding = FragmentSearchRepositoriesBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = this@SearchRepositoriesFragment.viewModel
        }
        context ?: return binding.root
        binding.recyclerView.adapter = adapter
        setupUI(adapter, binding)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    private fun setupUI(adapter: RepoAdapter, binding: FragmentSearchRepositoriesBinding) {
//        viewModel.repos.observe(viewLifecycleOwner) {
//            val data = it
//            binding.hasItem = data.isNotEmpty()
//            Log.i(LOG_TAG, "${data.size}")
//            adapter.submitList(data)
//        }
//
//        viewModel.networkErrors.observe(viewLifecycleOwner) {
//            Toast.makeText(requireContext(), "\uD83D\uDE28 Wooops $it", Toast.LENGTH_LONG).show()
//        }

        viewModel.repos.observe(viewLifecycleOwner) {
            if (it.isSuccess) {
                val data = it.getOrDefault(emptyList())
                binding.hasItem = data.isNotEmpty()
                adapter.submitList(data)
            }
        }


    }


    companion object {
        private const val LOG_TAG = "SearchRepositoriesFragment"
    }

}

