package com.sapo.android_paginng

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.map
import androidx.paging.PagedList
import kotlinx.coroutines.runBlocking

class RepoBoundaryCallback (
    private val query: String,
    private val services: GithubRemoteServices,
    private val cache: GithubLocalCache
) : PagedList.BoundaryCallback<Repo>() {

    companion object {
        private const val NETWORK_PAGE_SIZE = 50
    }

    private var isRequestInProcess  = false
    private var lastRequestedPage = 1

    private val _networkErrors = MutableLiveData<String>()
    // LiveData of network errors.
    val networkErrors: LiveData<String>
        get() = _networkErrors



    override fun onZeroItemsLoaded() {
        super.onZeroItemsLoaded()
        requestAndSaveData(query)
    }

    override fun onItemAtEndLoaded(itemAtEnd: Repo) {
        super.onItemAtEndLoaded(itemAtEnd)
        requestAndSaveData(query)
    }


    private fun requestAndSaveData(query: String) {
        if (isRequestInProcess) return
        isRequestInProcess = true

        val liveDataResult = services.search()
        Transformations.map(liveDataResult) { result ->
            isRequestInProcess = false
            if (result != null && result.isSuccess) {
                runBlocking {
                    cache.insert(result.getOrNull() ?: emptyList())
                }
            } else if (result != null && result.isFailure) {
                _networkErrors.postValue(result.exceptionOrNull()?.message ?: "")
            }
        }

    }

}